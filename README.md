# meta-webkit

This repository contains a set of Yocto/OpenEmbedded layers which provide additional recipes to make WebKit on OpenEmbedded-based systems.

## recipes-browser

This recipe contains WebKitGTK library and its browser.
